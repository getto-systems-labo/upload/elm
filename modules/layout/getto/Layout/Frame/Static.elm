module GettoCodes.Layout.Frame.Static exposing
  ( Flags
  , Model
  , Project
  , Page
  , Now
  , init
  , project
  , page
  , version
  )
import GettoCodes.View.Href as Href

import Time

type alias Flags =
  { project : Project
  , page    : PageFlags
  }

type alias PageFlags =
  { path : String
  , now :
    { posix : Int
    , zone  : Int
    }
  }

type Model = Model
  { version : Version
  , project : Project
  , page    : Page
  }

type alias Version =
  { copyright : String
  , version   : String
  }

type alias Project =
  { name    : String
  , company : String
  , title   : String
  , sub     : String
  }

type alias Page =
  { path : Href.Path
  , now  : Now
  }

type alias Now =
  { posix : Time.Posix
  , zone  : Time.Zone
  }

init : Version -> Flags -> Model
init versionData flags = Model
  { version = versionData
  , project = flags.project
  , page    =
    { path = flags.page.path |> Href.Internal
    , now =
      { posix = flags.page.now.posix |> Time.millisToPosix
      , zone  = Time.customZone flags.page.now.zone []
      }
    }
  }

project : Model -> Project
project (Model model) = model.project

page : Model -> Page
page (Model model) = model.page

version : Model -> Version
version (Model model) = model.version
